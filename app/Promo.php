<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Apprenant;

class Promo extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'start',
        'end'
    ];

    public function apprenants()
    {
        return $this->hasMany(Apprenant::class);
    }
}
