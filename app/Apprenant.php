<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Promo;

class Apprenant extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'firstname',
        'name',
        'email',
        'url'
    ];

    public function promo()
    {
        return $this->belongsTo(Promo::class);
    }
}
