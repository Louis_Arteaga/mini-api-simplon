<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
// $factory->define(App\Promo::class, function (Faker\Generator $faker) {

//     return [
//         'title' => $faker->title,
//         'start' => $faker->date,
//         'end' => $faker->date,
//         //    'apprenant_id' => factory('App\Apprenant')->create()
//     ];
// });

// $factory->define(App\Apprenant::class, function (Faker\Generator $faker) {

//     return [
//         'firstname' => $faker->firstName,
//         'name' => $faker->lastName,
//         'email' => $faker->email,
//         'url' => $faker->url,
//         'promo_id' => 1
//     ];
// });
