<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Promo;
use App\Apprenant;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $apprenant = new Apprenant;
            $apprenant->firstname = $faker->firstName;
            $apprenant->name = $faker->lastName;
            $apprenant->email = $faker->unique()->email;
            $apprenant->url = $faker->url;
          //  $apprenant->promo_id = 1;
            $apprenant->save();
        }

        for ($x = 0; $x < 1; $x++) { 
            $promo = new Promo;
            $promo->title = $faker->title;
            $promo->start = $faker->date;
            $promo->end = $faker->date;
            $promo->save();
        }
      //  DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        // Promo::truncate();
        // Apprenant::truncate();

        // $promoQuantity = 1;
        // $apprenantQuantity = 18;

        // factory(Promo::class, $promoQuantity)->create();
        // factory(Apprenant::class, $apprenantQuantity)->create();

       // DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); 


       
       // MEL :
    //    DB::table('apprenants')->insert([
    //     'firstname' => 'bloup',
    //     'name' => 'blip',
    //     'email' => 'sam@sam.com',
    //     'url' => 'http://bloup',
    //     'promo_id' => 1
    //    ]);

    //    DB::table('promos')->insert([
    //     'title' => 'bloup',
    //     'start' => 'blip',
    //     'end' => 'sam@sam.com'
    //    ]);
    }
}
